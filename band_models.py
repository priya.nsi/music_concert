from django.db import models

# Create your models here.
class BandMember(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    name = models.CharField(max_length=20)
    gender = models.CharField(max_length=1,choices=GENDER_CHOICES)
    date_of_birth = models.DateField(max_length=20)  
    instrument = models.CharField(max_length=20)  
    phone = models.CharField(max_length=10)  
    address= models.TextField(max_length=20) 
    def __str__(self):
        return self.name



class MemberList(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=40)


class MemberPost(models.Manager):
    title = models.CharField(max_length=40)
    image = models.ImageField(blank=False)
    comment = models.TextField(max_length=50,blank=True)


    
