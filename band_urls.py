from django.urls import path
from .views import MemberView ,PageView, PostView

app_name = "band_app"

urlpatterns = [
    path('page/',PageView.as_view(),name='page'),
    path('bandMember/',MemberView.as_view(),name='bandMember'),
    path('post/',PostView.as_view(),name='post'),
   
   ]


