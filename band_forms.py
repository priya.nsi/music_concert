from django.db import models
from django import forms 
from .models import BandMember ,MemberPost

class band_member(forms.ModelForm):
    class Meta():
        model = BandMember
        fields = ["name","gender","date_of_birth","instrument","phone","address"]

class Post_Form(forms.ModelForm): 
    class Meta():
        model = MemberPost
        fields = ["title","image","comment"]       
