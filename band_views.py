from django.shortcuts import render
from .forms import band_member ,Post_Form
from django.urls import reverse_lazy
from django.views.generic import FormView ,TemplateView
from . import views


# Create your views here.

class MemberView(FormView):
    form_class = band_member
    success_url = reverse_lazy("post") 
    template_name = "bandMember.html"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

class PageView(TemplateView):
    template_name = "temp.html"   

class PostView(FormView):
    form_class = Post_Form
    success_url = reverse_lazy("home") 
    template_name = "post.html"



